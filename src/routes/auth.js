const { Router } = require("express");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const cookie = require("cookie");

const User = require("../models/user");
const { checkStringIsEmpty } = require("../utils");
const authMiddleware = require("../middlewares/auth");

const authRouter = Router();

authRouter.post("/register", async (req, res) => {
  try {
    const { email, password, confirmPassword, firstName, lastName } = req.body;

    // check if the user has sent all the information necessary
    if (
      checkStringIsEmpty(email) ||
      checkStringIsEmpty(password) ||
      checkStringIsEmpty(confirmPassword) ||
      checkStringIsEmpty(firstName) ||
      checkStringIsEmpty(lastName)
    ) {
      return res.status(400).json({ error: "All fields are mandatory" });
    }

    // check that the password and confirmPassword match
    if (password !== confirmPassword) {
      return res.status(400).json({ error: "Passwords must match" });
    }

    // create a hash for this password
    const hashedPassword = await bcrypt.hash(password, 3);

    // create new user and save it
    const newUser = new User({
      email,
      password: hashedPassword,
      firstName,
      lastName,
    });
    await newUser.save();

    // send back to the user his information
    return res.status(201).json({
      email,
      firstName,
      lastName,
    });
  } catch (err) {
    return res.status(500).json({ error: "Something went wrong..." });
  }
});

authRouter.post("/login", async (req, res) => {
  try {
    const { email, password } = req.body;

    // check if the user has sent all necessary information
    if (checkStringIsEmpty(email) || checkStringIsEmpty(password)) {
      return res.status(400).json({ error: "All fields are mandatory" });
    }

    // try to find a user in the DB with that email
    const user = await User.findOne({ email });

    // if there's no user, it means that email is invalid
    if (!user) {
      return res.status(401).json({ error: "Wrong email or password" });
    }

    // compare the password sent by the user with the hashed one stored in the DB
    const arePasswordMatching = await bcrypt.compare(password, user.password);

    // error if they are not matching
    if (!arePasswordMatching) {
      return res.status(401).json({ error: "Wrong email or password" });
    }

    // extract all the information about the user except for password
    const { firstName, lastName, isAdmin } = user;

    // there's the tricky part
    // when a user logs in we want to create a JWT token for him that includes information, such as:
    // email, first name, last name and if the user's an admin.
    // after the token creation, we are going to send the token set as a cookie, which will
    // stored in the client browser
    const token = jwt.sign(
      { email, firstName, lastName, isAdmin },
      process.env.JWT_SECRET
    );
    res.set(
      "Set-Cookie",
      cookie.serialize("token", token, {
        httpOnly: true,
        secure: process.env.NODE_ENV === "production",
        sameSite: "lax",
        maxAge: 3600 * 24 * 30,
        path: "/",
      })
    );

    return res.status(200).json({
      email,
      firstName,
      lastName,
      isAdmin,
    });
  } catch (err) {
    return res.status(500).json({ error: "Something went wrong..." });
  }
});

authRouter.get("/logout", authMiddleware, (_, res) => {
  // the logout acts as it follows:
  // the user already must already be logged in and have a cookie
  // we just overwrite the value of the cookie to an empty string ""
  // and set it's expiry date in a past date, such that it will automatically delete
  // from the users browser
  res.set(
    "Set-Cookie",
    cookie.serialize("token", "", {
      httpOnly: true,
      expires: new Date(0),
      path: "/",
    })
  );

  return res.status(200).json({ success: true });
});

// this routes returns the current user from the token created
// since we already have all the necessary information we don't need to do refetch it
// from the database
// authMiddleware VERIFIES THE INTEGRITY of the token!
authRouter.get("/me", authMiddleware, (_, res) => {
  return res.status(200).json({
    ...res.locals.user,
  });
});

module.exports = authRouter;
