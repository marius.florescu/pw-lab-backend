const jwt = require("jsonwebtoken");

// a middleware before the actual request which means
// that the middleware can decide wether this request should go through or not
// in this case, the middleware takes the cookie from the request (if exists)
// and verifies its integrity.
// if everything is fine, it calls the next() function
// the next() function forwards all this information to the request we initially wanted
// but with additional information in our case (middleware sets some information) for the next request
const authMiddleware = (req, res, next) => {
  try {
    const token = req.cookies.token;
    if (!token) {
      return res.status(401).json({ error: "Not authenticated" });
    }

    const { email, firstName, lastName, isAdmin } = jwt.verify(
      token,
      process.env.JWT_SECRET
    );

    res.locals.user = {
      email,
      firstName,
      lastName,
      isAdmin,
    };

    return next();
  } catch (err) {
    return res.status(401).json({ error: "Not authenticated" });
  }
};

module.exports = authMiddleware;
