# PW Lab Backend

### How to run

- you need to have (Node.js)[https://nodejs.org/en] installed.
- clone the repository: `git clone https://gitlab.upt.ro/marius.florescu/pw-lab-backend`
- install the dependencies: `npm install`
- start the local server: `npm run dev`

If everything went well, you should see in the console the following message: `Backend listening in port 8080`.

## Lab 1

The scope of the Lab 1 (backend) is to create a simple backend with 2 API endpoints powered by (Express.js)[https://expressjs.com/].
At the end of the laboratory, everyone should be able to start the backend and make requests to the backend.

The created endpoints are:

- GET request on `http://localhost:8080/hello` -> which will return a JSON object with the value

```json
{
  "hello": true
}
```

- GET request on `http://localhost:8080/hello/:id` (replace `:id` with any string you'd like) -> which will return the following JSON object:

```json
{
  "id": ":id"
}
```

## Lab 2

The scope of the Lab 2 (backend) is to connect to a MongoDB database and to create our authentication mechanism.

For the database we have used MongoDB and the `mongoose` package. Each collection of documents has a model, that can be seen inside the `src/models` folder. At the moment, we only have the user model that has the following shape: `email`, `password` (**hashed**), `firstname`, `lastname` and `isAdmin`.

In our `src/routes` folder, we can see a new file called `auth.js`. This is the place where the routes for login, register and me exist.

- the register router can be accessed via a `POST` request to `http://localhost:8080/api/register` with the following request body:

```JSON
{
  "email": "marius.test@mail.me",
  "password": "Test1234!",
  "confirmPassword": "Test1234!",
  "firstName": "Marius",
  "lastName": "Florescu
}
```

An important note about the register route: after we do all the checks that the user sent all required information, that `password` and `confirmPassword` match, we **must** encrypt the password before storing it into the database for security reasons.

- the login route can be accessed via a `POST` request to `http://localhost:8080/api/login` with the following request body:

```JSON
{
  "email": "marius.test@mail.me",
  "password": "Test1234!",
}
```

Since we've stored the user password as a hashed string, the way we validate that the passwords are matching is by doing the following process: we get the user from the database with the password hased and we hash the password that our user sent. If those 2 strings match, it means that the user can be logged in. We **can't** decrypt passwords since the encryption functions are called _one way functions_, which means they are not reversible.
In order to persist our authentication, we are going to create a JWT (json web token) and set it as a cookie.

- the me route can be accessed via a `GET` request to `http://localhost:8080/api/me`. This will read the cookie, validate that the token is valid and return the user information.
